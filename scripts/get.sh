echo '===== pod ====='
kubectl get pod
echo '\n===== service ====='
kubectl get service
echo '\n===== deploy ====='
kubectl get deploy
echo '\n===== ingress ====='
kubectl get ingress
